import React, { Component } from 'react';

class Hero extends Component {

  render() {

    return (

        <div className="c-hero c-hero--cooltrax-app">

          <div className="o-wrapper">
            <h1><span>Cooltrax App</span></h1>
          </div>

        </div>

    )

  }

}

export default Hero;
