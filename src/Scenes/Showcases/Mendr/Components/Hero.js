import React, { Component } from 'react';

class Hero extends Component {

  render() {

    return (

        <div className="c-hero c-hero--mendr">

          <div className="o-wrapper">
            <h1><span>Mendr</span></h1>
          </div>

        </div>

    )

  }

}

export default Hero;
