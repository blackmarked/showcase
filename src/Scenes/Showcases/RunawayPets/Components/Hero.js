import React, { Component } from 'react';

class Hero extends Component {

  render() {

    return (

        <div className="c-hero c-hero--runaway">

          <div className="o-wrapper">
            <h1><span>Runaway Pets</span></h1>
          </div>

        </div>

    )

  }

}

export default Hero;
